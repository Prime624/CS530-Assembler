﻿#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include "opcodetab.h"
#include "opcode_error_exception.h"

using namespace std;

int main(int argc, char *argv[]) {
	try {
        opcodetab table;
        vector<string> codes_for_testing;
        codes_for_testing.push_back("ADD");
        codes_for_testing.push_back("aDd");
        codes_for_testing.push_back("norm");
        codes_for_testing.push_back("mul");
        codes_for_testing.push_back("lps");
        codes_for_testing.push_back("or");
        codes_for_testing.push_back("+or");
        
        for (unsigned int i = 0; i < codes_for_testing.size(); i++) {
			cout << endl << "Testing OPCode: " << codes_for_testing[i] << endl;
			cout << table.get_machine_code(codes_for_testing[i]) << endl;
			cout << table.get_instruction_size(codes_for_testing[i]) << endl;
		}
	}
	catch(opcode_error_exception excpt) {
		cout << "**Sorry, error " << excpt.getMessage() << endl;
	}
}
