/*	symtab.cc
 * 	CS530, Fall 2017
 */

#include "symtab.h"
#include "symtab_exception.h"

using namespace std;

string symtab::get_address_for(string symbol) {
	symbol = to_upper(symbol);
	symbols_iterator = symbols.find(symbol);
	if (symbols_iterator == symbols.end()) {
		stringstream sstream;
		sstream << symbol << " has not been assigned an address.";
		throw symtab_exception(sstream.str());
	}
	return symbols_iterator->second;
}

void symtab::set_symbol_address(string symbol, string address) {
	symbols[to_upper(symbol)] = address;
}

string symtab::to_upper(string input) {
	stringstream sstream;
	for (unsigned int i = 0; i < input.length(); i++) {
		sstream << char(toupper(input[i]));
	}
	return sstream.str();
}


void symtab::add_to_symbol(string a, string b, string c){

      
        symbols2[a] = make_pair(b, c);
 
}
