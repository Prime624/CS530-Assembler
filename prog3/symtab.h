/*	symtab.h
 * 	CS530, Fall 2017
 */

#ifndef SYMTAB_H
#define SYMTAB_H

#include <sstream>
#include <iostream>
#include <string>
#include <map>

using namespace std;

class symtab {
	public:
        void add_to_symbol(string, string, string);
		string get_address_for(string);
		void set_symbol_address(string, string);

	private:
		string to_upper(string);
		map<string, string> symbols;
		map<string, string> :: iterator symbols_iterator;
};

#endif
