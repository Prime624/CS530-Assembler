/*  sicxe_asm.h
	CS530, Spring 2016
*/

#ifndef SICXE_ASM_H
#define SICXE_ASM_H

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <sstream>
#include "file_parser.h"
#include "file_parse_exception.h"
#include "opcodetab.h"
#include "symtab.h"

using namespace std;

class sicxe_asm {
	
    public:
		sicxe_asm(string);
		void first_pass();
        void write_list_file();
		
    private:
        string to_upper(string);
        string to_string(int);
		
		file_parser parser;
		string file_loc;
		string program_name;
        int format;
        string op;
        std::vector<int> v;
        int counter;
        opcodetab findop;
		symtab symbols;
};

#endif
