/*  opcode_error_exception.h
	CS530
 */

#ifndef OPCODE_ERROR_EXCEPTION_H
#define OPCODE_ERROR_EXCEPTION_H

#include <stdbool.h>
#include <string>

using namespace std;

class opcode_error_exception {
public:

	opcode_error_exception(string s) {
		message = s;
	}

	string getMessage() {
		return message;
	}
private:
	string message;
};

#endif
