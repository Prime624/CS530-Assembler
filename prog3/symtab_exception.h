/*  symtab_exception.h
	CS530
 */

#ifndef SYMTAB_EXCEPTION_H
#define SYMTAB_EXCEPTION_H

#include <stdbool.h>
#include <string>

using namespace std;

class symtab_exception {
public:

	symtab_exception(string s) {
		message = s;
	}

	string getMessage() {
		return message;
	}
private:
	string message;
};

#endif
