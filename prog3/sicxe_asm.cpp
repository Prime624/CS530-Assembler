/*	sicxe_asm.cc
 * 	CS530, Fall 2017
 */

#include <iostream>
#include "sicxe_asm.h"
#include <fstream>
#include <iomanip>

using namespace std;

int main(int argc, char *argv[]) {

	if (argc != 2) {
		cout << "Error, you must supply the name of the file " <<
				"to process at the command line." << endl;
		exit(1);
	}

	string filename = argv[1];
	sicxe_asm sic(filename);
	sic.first_pass();
	sic.write_list_file();
}

sicxe_asm::sicxe_asm(string filename) {
	file_loc = filename;
	program_name = filename.substr(0, filename.find_last_of("."));
	
	parser.init(file_loc);
	try {
		parser.read_file();
	} catch (file_parse_exception excpt) {
		cout << "**Sorry, error " << excpt.getMessage() << endl;
		exit(2);
	}
	parser.print_file();
}

void sicxe_asm::first_pass() {
	unsigned int i = 0;
	// finds the memory start location
	while (parser.get_token(i, 1) != "start") {
		i++;
	}

	// gets the memory start location and makes it into an integer and assigns it to counter
    stringstream start(parser.get_token(i, 2));
	start >> counter;
	cout << " counter is " << counter << endl;
	v.push_back(0);
	i++; // Move to the next line after the start

	// finds the begining of the user defined tokens while assigning memory addresses to each line
	while (parser.get_token(i, 0).find(".") != 0) {
		op = to_upper(parser.get_token(i, 1));
		cout << "opcode is " << op << endl;
		
		// If the label token is not empty, add a symbol to the symtab
		if (parser.get_token(i, 0) != "") {
			symbols.set_symbol_address(parser.get_token(i, 0), to_string(counter));
			cout << "Set symbol: " << parser.get_token(i, 0) << " to " << symbols.get_address_for(parser.get_token(i, 0)) << endl;
		}
		
		if (op == "EQU") {
			v.push_back(counter);
			i++;
		} else if (op == "BASE"){ 
			v.push_back(counter);
			i++;
		} else if (op == "") {
			v.push_back(counter);
			i++;
		} else if (op == ".") {
			v.push_back(counter);
			i++;
		} else {
			format = findop.get_instruction_size(op);
			v.push_back(counter);
			counter = counter + format;
			i++;
		} // end of if else
	} // end of while

    i++;
    //adds storage allocation to the symbol table
   while (parser.get_token(i, 1) != "end"){
        symbols.add_to_symbol(parser.get_token(i, 0), parser.get_token(i, 1), parser.get_token(i, 2));
        i++; 
    } // end of while
 
    
    
    
    
    
}








void sicxe_asm::write_list_file() {
	unsigned int i = 0;
    ofstream list_file;
	stringstream sstream;
	sstream << program_name << ".lis";
	string list_file_name = sstream.str();
	list_file.open(sstream.str().c_str());
	
	list_file << setw(40) << "\t\t**" << file_loc << "**" << endl << endl;
	list_file << "LINE#" << setw(20) << "Address" << setw(20) << "Label" << setw(20) << "Opcode" << setw(20) << "Operand" << endl;
	
    list_file << "*****" << setw(20) << "*******" << setw(20) << "*****" << setw(20) << "******" << setw(20) << "*******" << endl;

    // skips comments at the first
    while(parser.get_token(i, 1) == "" || parser.get_token(i, 0).substr(0,1) == "."  || parser.get_token(i, 1).substr(0,1) == "."){
            i++;
        }
    
    // prints after the comments at start
    for (unsigned int b = 0; b < v.size(); b++) {
		
		
        list_file << setw(5) << b + 1 << setw(20) << v[b] << setw(20) << parser.get_token(i, 0) << setw(20) << parser.get_token(i, 1) << setw(20) << parser.get_token(i, 2) << endl;
	i++;
    
    }
	
    int s = v.size();
    int locadd = v[s-1];
    //finds the storage allocations section
    while (parser.get_token(s, 0).find(".") != 0){

        list_file << setw(5) << s + 1 << setw(20) << locadd << setw(20) << "    " << setw(20) << "    " << setw(20) << "    " << endl;
        s++;
    }
 
        s = s+2;
     // prints the storage allocations section   
    while (parser.get_token(s, 1) != "end") {
		
       list_file << setw(5) << s - 1 << setw(20) << "    " << setw(20) << parser.get_token(s, 0) << setw(20)   << parser.get_token(s, 1) << setw(20)<< parser.get_token(s, 2) << endl;
        s++;
        
	}
// prints the last line
list_file << setw(5) << s - 1 << setw(20) << "    " << setw(20) << parser.get_token(s, 0) << setw(20)   << parser.get_token(s, 1) << setw(20)<< parser.get_token(s, 2) << endl;
        s++;

    list_file.close();
}

string sicxe_asm::to_upper(string input) {
	stringstream sstream;
	for (unsigned int i = 0; i < input.length(); i++) {
		sstream << char(toupper(input[i]));
	}
	return sstream.str();
}

string sicxe_asm::to_string(int input) {
	stringstream sstream;
	sstream << input;
	return sstream.str();
}