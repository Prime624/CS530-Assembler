﻿/*  opcodetab.cc
    CS530, Fall 2017
*/

#include <cstdlib>
#include <string>
#include <sstream>
#include <map>
#include <iostream>
#include "opcodetab.h"
#include "opcode_error_exception.h"

using namespace std;

opcodetab::opcodetab() {

	codes["ADD"] = make_pair(3, "18");
	codes["ADDF"] = make_pair(3, "58");
	codes["ADDR"] = make_pair(2, "90");
	codes["AND"] = make_pair(3, "40");
	codes["CLEAR"] = make_pair(2, "B4");
	codes["COMP"] = make_pair(3, "28");
	codes["COMPF"] = make_pair(3, "88");
	codes["COMPR"] = make_pair(2, "A0");
	codes["DIV"] = make_pair(3, "24");
	codes["DIVF"] = make_pair(3, "64");
	codes["DIVR"] = make_pair(2, "9C");
	codes["FIX"] = make_pair(1, "C4");
	codes["FLOAT"] = make_pair(1, "C0");
	codes["HIO"] = make_pair(1, "F4");
	codes["J"] = make_pair(3, "3C");
	codes["JEQ"] = make_pair(3, "30");
	codes["JGT"] = make_pair(3, "34");
	codes["JLT"] = make_pair(3, "38");
	codes["JSUB"] = make_pair(3, "48");
	codes["LDA"] = make_pair(3, "00");
	codes["LDB"] = make_pair(3, "68");
	codes["LDCH"] = make_pair(3, "50");
	codes["LDF"] = make_pair(3, "70");
	codes["LDL"] = make_pair(3, "08");
	codes["LDS"] = make_pair(3, "6C");
	codes["LDT"] = make_pair(3, "74");
	codes["LDX"] = make_pair(3, "04");
	codes["LPS"] = make_pair(3, "D0");
	codes["MUL"] = make_pair(3, "20");
	codes["MULF"] = make_pair(3, "60");
	codes["MULR"] = make_pair(2, "98");
	codes["NORM"] = make_pair(1, "C8");
	codes["OR"] = make_pair(3, "44");
	codes["RD"] = make_pair(3, "D8");
	codes["RMO"] = make_pair(2, "AC");
	codes["RSUB"] = make_pair(3, "4C");
	codes["SHIFTL"] = make_pair(2, "A4");
	codes["SHIFTR"] = make_pair(2, "A8");
	codes["SIO"] = make_pair(1, "F0");
	codes["SSK"] = make_pair(3, "EC");
	codes["STA"] = make_pair(3, "0C");
	codes["STB"] = make_pair(3, "78");
	codes["STCH"] = make_pair(3, "54");
	codes["STF"] = make_pair(3, "80");
	codes["STI"] = make_pair(3, "D4");
	codes["STL"] = make_pair(3, "14");
	codes["STS"] = make_pair(3, "7C");
	codes["STSW"] = make_pair(3, "E8");
	codes["STT"] = make_pair(3, "84");
	codes["STX"] = make_pair(3, "10");
	codes["SUB"] = make_pair(3, "1C");
	codes["SUBF"] = make_pair(3, "5C");
	codes["SUBR"] = make_pair(2, "94");
	codes["SVC"] = make_pair(2, "B0");
	codes["TD"] = make_pair(3, "E0");
	codes["TIO"] = make_pair(1, "F8");
	codes["TIX"] = make_pair(3, "2C");
	codes["TIXR"] = make_pair(2, "B8");
	codes["WD"] = make_pair(3, "DC");
}

string opcodetab::to_upper(string input) {
	stringstream sstream;
	for (unsigned int i = 0; i < input.length(); i++) {
		sstream << char(toupper(input[i]));
	}
	return sstream.str();
}

string opcodetab::get_machine_code(string lookup_val) {
	string lookup_val_functional = to_upper(lookup_val);
	if (lookup_val[0] == '+') {
		lookup_val_functional = lookup_val_functional.substr(1);
	}
	cout << lookup_val_functional << endl;

	codes_iterator = codes.find(lookup_val_functional);
	if (codes_iterator == codes.end()) {
		stringstream sstream;
		sstream << lookup_val << " is not a valid opcode.";
		throw opcode_error_exception(sstream.str());
	}
	return (codes_iterator->second).second;
}

int opcodetab::get_instruction_size(string lookup_val) {
	bool isFormat4 = false;
	string lookup_val_functional = to_upper(lookup_val);
	if (lookup_val[0] == '+') {
		isFormat4 = true;
		lookup_val_functional = lookup_val_functional.substr(1);
	}

	codes_iterator = codes.find(lookup_val_functional);
	if (codes_iterator == codes.end()) {
		stringstream sstream;
		sstream << lookup_val << " is not a valid opcode.";
		throw opcode_error_exception(sstream.str());
	}
	if (!isFormat4) {
		return (codes_iterator->second).first;
	} else {
		if ((codes_iterator->second).first == 3) {
			return 4;
		} else {
			stringstream sstream;
			sstream << lookup_val << " is not a valid opcode in Format 4.";
			throw opcode_error_exception(sstream.str());
		}
	}
}
