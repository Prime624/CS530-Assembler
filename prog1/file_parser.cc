/*  file_parser.cc
    CS530, Fall 2017
*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>
#include "file_parser.h"
#include "file_parse_exception.h"

using namespace std;

file_parser::file_parser(string fp) {
	file_path = fp;
}

// dtor
// ~file_parser();
file_parser::~file_parser() {
		// Do stuff here
}

// reads the source file, storing the information is some
// auxiliary data structure you define in the private section. 
// Throws a file_parse_exception if an error occurs.
// if the source code file fails to conform to the above
// specification, this is an error condition.     
void file_parser::read_file() {
	string delimiters = " \t\n";
	
	infile.open(file_path.c_str(), ios::in);
    if(!infile) {
		throw file_parse_exception("Error: File could not be opened.");
        //print_error("Sorry, could not open the file for reading");
	}
    
    int line_number = 0;
    while(!infile.eof()) {
		line_number++;
		//cout << endl << "Line: " << line_number << endl;
        getline(infile,line);
        //cout << endl << line << endl;
        int token_start = line.find_first_not_of(delimiters, 0);
        if (token_start == -1) {
			//cout << "''" << endl << "''" << endl << "''" << endl << "''" << endl;
			contents.push_back("");
			contents.push_back("");
			contents.push_back("");
			contents.push_back("");
			continue;
		}
		bool oneToken = false;
		bool isComment = false;
        int token_end = line.find_first_of(delimiters, token_start);
        if (token_end == -1) {
			oneToken = true;
		}
        if (token_start == 0) {
			string token = line.substr(token_start, token_end);
			if (token.substr(0, 1).compare(".") == 0) {
				token = line.substr(token_start);
				isComment = true;
			}
			if (! isComment) {
				//cout << token[0] << endl;
				if (token[0] < 65 || token[0] > 122) {
					stringstream sstream;
					sstream << "Invalid first character of label, line " << line_number;
					throw file_parse_exception(sstream.str());
				}
				for (unsigned int i = 0; i < token.length(); i++) {
					if (! ((token[i] >= 65 && token[i] <= 122) || (token[i] >= 48 && token[i] <= 57))) {
						stringstream sstream;
						sstream << "Invalid characters in label, line " << line_number;
						throw file_parse_exception(sstream.str());
					}
				}
			}
			contents.push_back(token);
			//cout << "'" << token << "'" << endl;
			if (oneToken || isComment) {
				//cout << "''" << endl << "''" << endl << "''" << endl;
				contents.push_back("");
				contents.push_back("");
				contents.push_back("");
				continue;
			}
			line = line.substr(token_end);
			token_start = line.find_first_not_of(delimiters, 0);
			token_end = line.find_first_of(delimiters, token_start);
		} else {
			//cout << "''" << endl;
			contents.push_back("");
		}
		
		int token_counter = 1;
        while (token_start != -1) {
			token_counter++;
			string token = line.substr(token_start, token_end - token_start);
			if (token.substr(0, 1).compare(".") == 0) {
				token = line.substr(token_start);
				token_end = -1;
			}
			int open_quote = token.find_first_of("'", 0);
			if (open_quote != -1) {
				//cout << "HERE" << endl;
				int end_quote = line.find_first_of("'", token_start + open_quote + 1);
				//cout << line_number << " : " << end_quote << endl;
				//cout << token_start << open_quote << end_quote << endl;
				int third_quote = line.find_first_of("'", end_quote + 1);
				//cout << "Third Quote: " << third_quote << endl;
				if (third_quote != -1) {
					stringstream sstream;
					sstream << "More than 2 single quotes found, line " << line_number;
					throw file_parse_exception(sstream.str());
				}
				token_end = line.find_first_of(delimiters, end_quote);
				token = line.substr(token_start, token_end - token_start);
				//cout << token << endl;
			}
			//cout << "'" << token << "'" << endl;
			if (token_counter == 4 && token[0] != '.') {
				stringstream sstream;
				sstream << "Column 4 must be a comment, line " << line_number;
				throw file_parse_exception(sstream.str());
			}
			contents.push_back(token);
			if (token_end == -1) {
				while (token_counter < 4) {
					token_counter++;
					//cout << "''" << endl;
					contents.push_back("");
				}
				break;
			}
			line = line.substr(token_end);
			token_start = line.find_first_not_of(delimiters, 0);
			if (token_start == -1) {
				while (token_counter < 4) {
					token_counter++;
					//cout << "''" << endl;
					contents.push_back("");
				}
				break;
			}
			token_end = line.find_first_of(delimiters,  token_start);
		}
    }
    infile.close();
}   

// returns the token found at (row, column).  Rows and columns
// are zero based.  Returns the empty string "" if there is no 
// token at that location. column refers to the four fields
// identified above.
string file_parser::get_token(unsigned int row, unsigned int column) {
	return contents.at((row * 4) + column);
}

// prints the source code file to stdout.  Should not repeat 
// the exact formatting of the original, but uses tabs to align
// similar tokens in a column. The fields should match the 
// order of token fields given above (label/opcode/operands/comments)
void file_parser::print_file() {
	for(unsigned int i=0; i < contents.size(); i+=4) {
		cout << contents[i] << "\t" << contents[i+1] << "\t" << contents[i+2] << "\t" << contents[i+3] << endl;
	}
}

// returns the number of lines in the source code file
//int size();
int file_parser::size() {
	return contents.size() / 4;
}
